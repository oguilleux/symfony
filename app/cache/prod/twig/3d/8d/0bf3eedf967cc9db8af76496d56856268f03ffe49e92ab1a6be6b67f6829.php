<?php

/* ::base.html.twig */
class __TwigTemplate_3d8d0bf3eedf967cc9db8af76496d56856268f03ffe49e92ab1a6be6b67f6829 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'nav' => array($this, 'block_nav'),
            'body' => array($this, 'block_body'),
            'sidebar' => array($this, 'block_sidebar'),
            'footer' => array($this, 'block_footer'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <meta name=\"viewport\" content=\"width-device-width, initial-scale=1.0\" />
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "Blog Pixel Panda</title>

        ";
        // line 8
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 9
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
        <link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/bootstrap.min.css"), "html", null, true);
        echo "\" />
        <link href=\"//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css\" rel=\"stylesheet\">
        <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("css/style.css"), "html", null, true);
        echo "\" />

    </head>
    <body>
        
        ";
        // line 17
        $this->displayBlock('nav', $context, $blocks);
        // line 18
        echo "        <div class=\"container\">
            <section class=\"main-col col-md-8\">
                ";
        // line 20
        $this->displayBlock('body', $context, $blocks);
        // line 21
        echo "            </section>
            <aside class=\"sidebar col-md-4\">
                ";
        // line 23
        $this->displayBlock('sidebar', $context, $blocks);
        // line 24
        echo "            </aside>

            <div id=\"footer\">
                ";
        // line 27
        $this->displayBlock('footer', $context, $blocks);
        // line 30
        echo "            </div>
        </div>
        ";
        // line 32
        $this->displayBlock('javascripts', $context, $blocks);
        // line 33
        echo "        <script src=\"//code.jquery.com/jquery.min.js\" type=\"text/javascript\"></script>
        <script src=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("js/bootstrap.min.js"), "html", null, true);
        echo "\" type=\"text/javascript\"></script>
    </body>
</html>
";
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
    }

    // line 8
    public function block_stylesheets($context, array $blocks = array())
    {
    }

    // line 17
    public function block_nav($context, array $blocks = array())
    {
        $this->env->loadTemplate("::nav.html.twig")->display($context);
    }

    // line 20
    public function block_body($context, array $blocks = array())
    {
    }

    // line 23
    public function block_sidebar($context, array $blocks = array())
    {
    }

    // line 27
    public function block_footer($context, array $blocks = array())
    {
        // line 28
        echo "                    
                ";
    }

    // line 32
    public function block_javascripts($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  130 => 32,  125 => 28,  122 => 27,  117 => 23,  112 => 20,  106 => 17,  101 => 8,  96 => 6,  88 => 34,  85 => 33,  83 => 32,  79 => 30,  77 => 27,  70 => 23,  66 => 21,  58 => 17,  50 => 12,  45 => 10,  38 => 8,  33 => 6,  26 => 1,  87 => 25,  78 => 21,  72 => 24,  64 => 20,  60 => 18,  55 => 12,  47 => 9,  40 => 9,  37 => 6,  31 => 5,  28 => 4,);
    }
}
