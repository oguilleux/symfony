<?php

/* ::nav.html.twig */
class __TwigTemplate_88ef023a2fd47d9f6d196138a59f0e468f70335216f3613a1dc437eff290b5f7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<nav class=\"navbar navbar-default\" role=\"navigation\">
  <div class=\"container\">
  <div class=\"container-fluid\">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class=\"navbar-header\">
      <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">
        <span class=\"sr-only\">Toggle navigation</span>
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
        <span class=\"icon-bar\"></span>
      </button>
      <a class=\"navbar-brand\" href=\"#\">Panda Blog</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">
      <ul class=\"nav navbar-nav\">
          <li><a href=\"";
        // line 18
        echo $this->env->getExtension('routing')->getPath("BloggerBlogBundle_homepage");
        echo "\">Accueil</a></li>
          <li><a href=\"";
        // line 19
        echo $this->env->getExtension('routing')->getPath("BloggerBlogBundle_about");
        echo "\">A propos</a></li>
          <li><a href=\"";
        // line 20
        echo $this->env->getExtension('routing')->getPath("BloggerBlogBundle_contact");
        echo "\">Contact</a></li>
      </ul>
    
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
  </div>
</nav>";
    }

    public function getTemplateName()
    {
        return "::nav.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 20,  42 => 19,  19 => 1,  130 => 32,  125 => 28,  122 => 27,  117 => 23,  112 => 20,  106 => 17,  101 => 8,  96 => 6,  88 => 34,  85 => 33,  83 => 32,  79 => 30,  77 => 27,  70 => 23,  66 => 21,  58 => 17,  50 => 12,  45 => 10,  38 => 18,  33 => 6,  26 => 1,  87 => 25,  78 => 21,  72 => 24,  64 => 20,  60 => 18,  55 => 12,  47 => 9,  40 => 9,  37 => 6,  31 => 5,  28 => 4,);
    }
}
